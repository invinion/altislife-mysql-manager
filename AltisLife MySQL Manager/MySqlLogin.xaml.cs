﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace INVINION
{
    /// <summary>
    /// Interaktionslogik für MySqlLogin.xaml
    /// </summary>
    public partial class MySqlLogin : Window
    {
        public MySqlLogin()
        {
            InitializeComponent();
            database.Text = GlobalData.MySqlLogin.getMySQLDataBase();
            host.Text = GlobalData.MySqlLogin.getMySQLHostName();
            pw.Password = GlobalData.MySqlLogin.getMySQLPassword();
            user.Text = GlobalData.MySqlLogin.getMySQLUser();
        }

        private void ok_Click(object sender, RoutedEventArgs e)
        {
            GlobalData.MySqlLogin.setMySQLDataBase(database.Text);
            GlobalData.MySqlLogin.setMySQLHostName(host.Text);
            GlobalData.MySqlLogin.setMySQLPassword(pw.Password);
            GlobalData.MySqlLogin.setMySQLUser(user.Text);
            this.DialogResult = true;
        }
    }
}
