﻿using INVINION.Lists;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace INVINION
{
    internal enum loadPlayerListOrder
    {
        orderByName,

        orderByID
    }

    internal class MySQLConnector
    {
        private MySqlCommand DB_Command;
        private MySqlConnection DB_Connection;

        public bool Connected { get; set; }

        public Exception LastException { get; set; }

        public List<PlayerListView> loadPlayerList(loadPlayerListOrder order)
        {
            string orderString = string.Empty;
            switch (order)
            {
                case loadPlayerListOrder.orderByName:
                    orderString = " ORDER BY name";
                    break;

                case loadPlayerListOrder.orderByID:
                    orderString = " ORDER BY playerid";
                    break;
            }

            List<PlayerListView> pl = new List<PlayerListView>();
            try
            {
                DB_Command.CommandText = "SELECT name, playerid FROM players" + orderString;
                MySqlDataReader reader;
                reader = DB_Command.ExecuteReader();
                while (reader.Read())
                {
                    pl.Add(new PlayerListView { name = reader.GetValue(0).ToString(), uid = reader.GetValue(1).ToString() });
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                LastException = ex;
                pl.Clear();
            }

            return pl;
        }

        public List<GangsListView> loadGags()
        {
            List<GangsListView> gl = new List<GangsListView>();
            try
            {
                DB_Command.CommandText = "SELECT owner, name, members, maxmembers FROM gangs ORDER BY id";
                MySqlDataReader reader;
                reader = DB_Command.ExecuteReader();
                while (reader.Read())
                {
                    string[] users = reader.GetValue(2).ToString().Replace("\"", "").Replace("]", "").Replace("[", "").Replace("`", "").Split(',');
                    List<string> userlist = new List<string>();
                    for (int i = 0; i < users.Length; i++)
                    {
                        userlist.Add(users[i]);
                    }
                    gl.Add(new GangsListView { owner = reader.GetValue(0).ToString(), name = reader.GetValue(1).ToString(), member = userlist, maxUser = reader.GetValue(3).ToString() });
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                LastException = ex;
                gl.Clear();
            }

            return gl;
        }

        public bool SQLConnect()
        {
            try
            {
                DB_Connection = new MySqlConnection("SERVER=" + GlobalData.MySqlLogin.getMySQLHostName() + ";DATABASE=" + GlobalData.MySqlLogin.getMySQLDataBase() + ";UID=" + GlobalData.MySqlLogin.getMySQLUser() + ";PASSWORD=" + GlobalData.MySqlLogin.getMySQLPassword() + ";");
                DB_Command = DB_Connection.CreateCommand();
                DB_Connection.Open();
                Connected = true;
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                LastException = ex;
                Connected = false;
                return false;
            }
        }

        public bool SQLDisConnect()
        {
            try
            {
                DB_Connection.Close();
                Connected = false;
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                LastException = ex;
                Connected = true;
                return false;
            }
        }
    }
}