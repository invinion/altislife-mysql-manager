﻿using System.Collections.Generic;

namespace INVINION.Lists
{
    internal class GangsListView
    {
        public GangsListView()
        {
            member = new List<string>();
        }

        public string maxUser { get; set; }

        public List<string> member { get; set; }

        public string name { get; set; }

        public string owner { get; set; }
    }

    internal class PlayerListView
    {
        public string name { get; set; }

        public string uid { get; set; }
    }
}