﻿namespace INVINION.GlobalData
{
    internal static class MySqlLogin
    {
        private static string UserName = "";
        private static string Password = "";
        private static string DataBase = "";
        private static string HostName = "";

        public static void setMySQLPassword(string pw)
        {
            Password = pw;
        }

        public static void setMySQLUser(string user)
        {
            UserName = user;
        }

        public static void setMySQLDataBase(string db)
        {
            DataBase = db;
        }

        public static void setMySQLHostName(string host)
        {
            HostName = host;
        }

        public static string getMySQLPassword()
        {
            return Password;
        }

        public static string getMySQLUser()
        {
            return UserName;
        }

        public static string getMySQLDataBase()
        {
            return DataBase;
        }

        public static string getMySQLHostName()
        {
            return HostName;
        }
    }
}