﻿using INVINION.Lists;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace INVINION
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            database = new MySQLConnector();
            gangs = new List<GangsListView>();
            GlobalData.MySqlLogin.setMySQLHostName("127.0.0.1");
            GlobalData.MySqlLogin.setMySQLUser("root");
            GlobalData.MySqlLogin.setMySQLPassword("none");
            GlobalData.MySqlLogin.setMySQLDataBase("database");
        }

        private MySQLConnector database;
        List<GangsListView> gangs;

        private void setServerInfo()
        {
            serverLoginData.Content = GlobalData.MySqlLogin.getMySQLHostName() + "@" + GlobalData.MySqlLogin.getMySQLUser() + "@" + GlobalData.MySqlLogin.getMySQLDataBase();
        }

        private void serverChange_Click(object sender, RoutedEventArgs e)
        {
            MySqlLogin login = new MySqlLogin();
            if(login.ShowDialog() == true)
            {
                setServerInfo();
            }
        }

        private void serverReload_Click(object sender, RoutedEventArgs e)
        {
            if(database.Connected == true)
            {
                loadPlayerListOrder order;
                if(sortName.IsChecked == true)
                {
                    order = loadPlayerListOrder.orderByName;
                }
                else
                {
                    order = loadPlayerListOrder.orderByID;
                }
                List<PlayerListView> pl = database.loadPlayerList(order);
                playerList.Items.Clear();
                foreach (PlayerListView item in pl)
                {
                    playerList.Items.Add(new PlayerListView { name = item.name, uid = item.uid });
                }
                gangs = database.loadGags();
            }
        }

        private void serverConnect_Click(object sender, RoutedEventArgs e)
        {
            if(database.SQLConnect() == false)
            {
                MessageBox.Show("Error at loading" + Environment.NewLine + database.LastException.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void serverDisConnect_Click(object sender, RoutedEventArgs e)
        {
            if (database.SQLDisConnect() == false)
            {
                MessageBox.Show("Error at loading" + Environment.NewLine + database.LastException.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void playerList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(playerList.SelectedIndex >= 0)
            {
                PlayerListView pl = (PlayerListView)playerList.SelectedItem;
                foreach (GangsListView gang in gangs)
                {
                    foreach (string member in gang.member)
                    {
                        if(member == pl.uid)
                        {
                            playerGang.Text = gang.name;
                            break;
                        }
                    }
                }
            }
        }
    }
}
